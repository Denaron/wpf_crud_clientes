﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Validation;

namespace WPF_CRUD_CLIENTES
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        WPF_CRUDEntities ctx = new WPF_CRUDEntities();
        public MainWindow()
        {
            InitializeComponent();
        }



        private void btnCadastrar_Click(object sender, RoutedEventArgs e)
        {

            Cliente objNovoCliente = new Cliente();

            objNovoCliente.nome = txbNome.Text;
            objNovoCliente.cpf = txbCpf.Text;
            objNovoCliente.endereco = txbEndereco.Text;
            objNovoCliente.data_nascimento = datePickerDtNascimento.SelectedDate;

            ctx.Cliente.Add(objNovoCliente);
            ctx.SaveChanges();

            MessageBox.Show("O cliente foi cadastrado com sucesso.");
            LimparCampos();
            listar();
        }



        private void LimparCampos()
        {
            txbID.Text = "";

            txbNome.Text = "";

            txbCpf.Text = "";

            txbEndereco.Text = "";

            datePickerDtNascimento.SelectedDate = DateTime.Now;

        }

        private void btnListar_Click(object sender, RoutedEventArgs e)
        {
            listar();
        }

        private void listar()
        {
            List<Cliente> objListaClientes = ctx.Cliente.ToList<Cliente>();

            dataGridClientes.ItemsSource = objListaClientes;
        }

        private void btnAlterar_Click(object sender, RoutedEventArgs e)
        {
            if (txbID.Text != String.Empty)
            {
                int idCliente = int.Parse(txbID.Text);
                Cliente objClienteAlterar = ctx.Cliente.Where(c => c.id_cliente == idCliente).FirstOrDefault();
                objClienteAlterar.nome = txbNome.Text;
                objClienteAlterar.cpf = txbCpf.Text;
                objClienteAlterar.endereco = txbEndereco.Text;
                objClienteAlterar.data_nascimento = datePickerDtNascimento.SelectedDate;

                ctx.Entry(objClienteAlterar).CurrentValues.SetValues(objClienteAlterar);
                ctx.SaveChanges();

                MessageBox.Show("O cliente foi alterado com sucesso!");

            }
            else
            {
                MessageBox.Show("Você deve selecionar um cliente.");
            }
            listar();
            LimparCampos();

        }

        private void dataGridClientes_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            Cliente objClienteSelecionado = (Cliente)dataGridClientes.SelectedItem;

            txbID.Text = objClienteSelecionado.id_cliente.ToString();
            txbNome.Text = objClienteSelecionado.nome;
            txbCpf.Text = objClienteSelecionado.cpf;
            txbEndereco.Text = objClienteSelecionado.endereco;
            datePickerDtNascimento.SelectedDate = objClienteSelecionado.data_nascimento;
        }

        private void btnExcluir_Click(object sender, RoutedEventArgs e)
        {

            if (txbID.Text != String.Empty)
            {
                int idCliente = int.Parse(txbID.Text);
                Cliente objClienteExcluir = ctx.Cliente.Where(c => c.id_cliente == idCliente).FirstOrDefault();


                ctx.Cliente.Remove(objClienteExcluir);
                ctx.SaveChanges();

                MessageBox.Show("O cliente foi excluido com sucesso!");

            }
            else
            {
                MessageBox.Show("Você deve selecionar um cliente.");
            }
            listar();
            LimparCampos();

        }

        private void btnPesquisar_Click(object sender, RoutedEventArgs e)
        {
            if (txbNome.Text != string.Empty)
            {
                List<Cliente> objListaClientes = ctx.Cliente.Where(c => c.nome.Contains(txbNome.Text)).ToList<Cliente>();
                dataGridClientes.ItemsSource = objListaClientes;
            }
            else
            {
                MessageBox.Show("Favor informar o nome do Cliente a ser pesquisado.");
            }
            LimparCampos();

        }

    }
}
